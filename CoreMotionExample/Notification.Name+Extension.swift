//
//  Notifications.swift
//  CoreMotionExample
//
//  Created by Leonardo Gómez Sosa on 15/04/20.
//  Copyright © 2020 Maxim Bilan. All rights reserved.
//

import Foundation


extension Notification.Name {
    static let isAccelerating = Notification.Name("isAccelerating")
    static let isRotating = Notification.Name("isRotating")
    static let readyTofocus = Notification.Name("readyTofocus")
}
