//
//  ViewController.swift
//  CoreMotionExample
//
//  Created by Maxim Bilan on 1/21/16.
//  Copyright © 2016 Maxim Bilan. All rights reserved.
//

import UIKit
import CoreMotion
import AVFoundation
import Vision

class ViewController: UIViewController {
    // MARK: - Outlets
    @IBOutlet var cameraHndlr: CameraViewHdlrVideo!
    @IBOutlet weak var movingLabel: UILabel!
    @IBOutlet weak var readyLabel: UILabel!
    @IBOutlet weak var preview: UIImageView!
    
    // MARK: - Vision Rectangle request
    var rectangleDetectionRequest: VNDetectRectanglesRequest {
        let vnRectReq = VNDetectRectanglesRequest(completionHandler: self.handleRectangles)
        vnRectReq.quadratureTolerance = 15.0
        vnRectReq.maximumObservations = 1
        return vnRectReq
    }
    
    // MARK: - Variables
    var outputImage: CIImage!
    let proportion = 1.0
    let rectanglesWithProportion = 0
    var image: CIImage!
    
    lazy var photoCaptureCompletionBlock: ((UIImage?) -> Void) = { (img) in
        if self.didTakePhoto {
            return
        }
        guard let ciImage = CIImage(image: img!)
            else { fatalError(":::  can't read image ") }
        guard let orientation = CGImagePropertyOrientation(rawValue: UInt32(img!.imageOrientation.rawValue))
            else {
                fatalError("::: can't detect orientation")
        }
        
        self.outputImage = ciImage.oriented(forExifOrientation: Int32(orientation.rawValue))
        
    }
    
    
    // MARK: - Flags y estados de la aplicacion
    var isMoving = true
    var didTakePhoto = false
    var detectedRectangle = false
    
    // MARK: - Ciclo de vida de la aplicacion
    override func viewDidLoad() {
        super.viewDidLoad()
        guard self.cameraHndlr != nil
            , self.cameraHndlr.setup() == true
            else { return }
        weak var delegate = self
        cameraHndlr.avDelegate = delegate
        self.cameraHndlr.startVideoCapture(delegate: self)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        MotionService.shared.stopMotionService()
        NotificationCenter.default.removeObserver(self, name: .isAccelerating, object: nil)
        NotificationCenter.default.removeObserver(self, name: .isRotating, object: nil)
        NotificationCenter.default.removeObserver(self, name: .readyTofocus, object: nil)
        cameraHndlr.avSession.stopRunning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideReady), name: .isRotating, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideReady), name: .isAccelerating, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showReady), name: .readyTofocus, object: nil)
        MotionService.shared.startMotionService()
        cameraHndlr.avSession.startRunning()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.cameraHndlr.avPrevLayer?.frame.size = self.cameraHndlr.cameraView.frame.size
        self.cameraHndlr.avPrevLayer?.videoGravity = .resizeAspectFill
    }
    
    
    // MARK: - Acciones en respuesta a notificaciones
    @objc func hideReady() {
        isMoving = true
        self.updateLabel()
        self.detectedRectangle = false
        self.didTakePhoto = false
    }
    
    @objc  func showReady() {
        isMoving = false
        self.updateLabel()
        if !self.didTakePhoto {
//            self.cameraHndlr.captureImage(completion: self.photoCaptureCompletionBlock, center: CGPoint(x: 400, y: 400))
            self.didTakePhoto = true
        }
    }
    
    func updateLabel(){
        DispatchQueue.main.async {
            self.movingLabel.isHidden = !self.isMoving
            self.readyLabel.isHidden = self.isMoving
        }
    }
    
}

// MARK: - AVCaptureVideoDataOutputSampleBufferDelegate
extension ViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard  let imageBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }
        if isMoving || detectedRectangle{
            return
        }
        let ciimage : CIImage = CIImage(cvPixelBuffer: imageBuffer)
        let image : UIImage = self.convert(cmage: ciimage)
        self.image = ciimage
        guard let orientation = CGImagePropertyOrientation(rawValue: UInt32(image.imageOrientation.rawValue))
            else {
                fatalError("--> can't detect orientation")
        }
        
        let handler = VNImageRequestHandler(ciImage: ciimage, orientation: CGImagePropertyOrientation(rawValue: UInt32(Int32(orientation.rawValue)))!)
        
        DispatchQueue.main.sync {
            do {
                try handler.perform([self.rectangleDetectionRequest])
            } catch {
                print("::: \(error)")
            }
        }
    }
    
    // Convert CIImage to CGImage
    func convert(cmage:CIImage) -> UIImage {
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        return image
    }
}
// MARK: - AVCapturePhotoCaptureDelegate
extension ViewController: AVCapturePhotoCaptureDelegate {
    public func photoOutput(_ captureOutput: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?,
                            resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Swift.Error?) {
        if error != nil {
            return
            
        } else if let buffer = photoSampleBuffer, let data = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: buffer, previewPhotoSampleBuffer: nil),
            let image = UIImage(data: data) {
            self.didTakePhoto = true
            let ciImage: CIImage = CIImage(cgImage: image.cgImage!).oriented(forExifOrientation: 6)
            let flippedImage = ciImage.transformed(by: CGAffineTransform(scaleX: -1, y: 1))
            let img = UIImage.convert(from: flippedImage)
            DispatchQueue.main.async {
                self.preview.image = img
            }
            
            //            self.photoCaptureCompletionBlock(img)
        } else {
            //            self.photoCaptureCompletionBlock(nil)
            
        }
    }
}

// MARK: - Tratamiento a rectángulos
extension ViewController {
    func handleRectangles(request: VNRequest, error: Error?) {
        guard let observations = request.results as? [VNRectangleObservation]
            else { fatalError("unexpected result type from VNDetectRectanglesRequest") }
        guard let detectedRectangle = observations.first else { print("no se detectaron rectangulos"); return }
        self.detectedRectangle = true
        print(detectedRectangle.description)
        
        let previewPoints: [CGPoint] = self.getScaledPoints(from: detectedRectangle, to: self.cameraHndlr.cameraView.frame.size)

        let filter = self.ciPerspectiveFilter(for: self.image!, with: previewPoints)
        if let output = filter.outputImage  {
            print(self.cameraHndlr.cameraView.frame.size, output.extent.size)
        } else {
            print ("--> no puede crear rectngulo")
        }
        
    }
    
    func ciPerspectiveFilter(for img: CIImage, with pointsReal:[CGPoint]) -> CIFilter{
        let perspectiveCorrection = CIFilter(name: "CIPerspectiveCorrection")!
        perspectiveCorrection.setValue(CIVector(cgPoint: pointsReal[0]),
                                       forKey: "inputTopLeft")
        perspectiveCorrection.setValue(CIVector(cgPoint: pointsReal[1]),
                                       forKey: "inputTopRight")
        perspectiveCorrection.setValue(CIVector(cgPoint: pointsReal[3]),
                                       forKey: "inputBottomRight")
        perspectiveCorrection.setValue(CIVector(cgPoint: pointsReal[2]),
                                       forKey: "inputBottomLeft")
        perspectiveCorrection.setValue(img,
                                       forKey: kCIInputImageKey)
        return perspectiveCorrection
    }
    
    func getScaledPoints(from detectedRectangle: VNRectangleObservation, to size: CGSize) -> [CGPoint] {
        var points: [CGPoint] = []
        points.append(detectedRectangle.topLeft.scaled(to: size))
        points.append(detectedRectangle.topRight.scaled(to: size))
        points.append(detectedRectangle.bottomLeft.scaled(to: size))
        points.append(detectedRectangle.bottomRight.scaled(to: size))
        return points
    }
    
    func getScaledMid(from detectedRectangle: VNRectangleObservation, to size: CGSize) -> CGPoint {
        var pointsX: [CGFloat] = []
        var pointsY: [CGFloat] = []
        pointsX.append(detectedRectangle.topLeft.scaled(to: size).x)
        pointsX.append(detectedRectangle.topRight.scaled(to: size).x)
        pointsX.append(detectedRectangle.bottomLeft.scaled(to: size).x)
        pointsX.append(detectedRectangle.bottomRight.scaled(to: size).x)
        pointsY.append(detectedRectangle.topLeft.scaled(to: size).y)
        pointsY.append(detectedRectangle.topRight.scaled(to: size).y)
        pointsY.append(detectedRectangle.bottomLeft.scaled(to: size).y)
        pointsY.append(detectedRectangle.bottomRight.scaled(to: size).y)
        let maxX = pointsX.max()!
        let maxY = pointsY.max()!
        let minX = pointsX.min()!
        let minY = pointsY.min()!
        return CGPoint(x: (maxX - minX) / 2 , y: (maxY - minY) / 2)
    }
}
