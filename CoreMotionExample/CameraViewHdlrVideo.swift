//
//  CameraView.swift
//  LaComerCD
//
//  Created by softtek on 13/02/20.
//  Copyright © 2020 LaComer. All rights reserved.
//

import UIKit
import AVFoundation

@available(iOS 10.0, *)
class CameraViewHdlrVideo : NSObject {
    
    var avSession: AVCaptureSession!
    var avDevice: AVCaptureDevice!
    var avInput: AVCaptureDeviceInput!
    var avPrevLayer: AVCaptureVideoPreviewLayer!
    var avCaptureImageOutput: AVCaptureVideoDataOutput!
    var photoOutput: AVCapturePhotoOutput?
    var avDelegate: AVCapturePhotoCaptureDelegate?
    
    @IBOutlet weak var versionLbl: UILabel!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    var torchMode = AVCaptureDevice.TorchMode.off
    var center:CGPoint!

    
    deinit {
        avDelegate = nil
        photoOutput = nil
        avCaptureImageOutput = nil
        avPrevLayer = nil
        avInput = nil
        avDevice = nil
        avSession = nil
        print("X deinit Captura caja")
    }
    
    func killCamera() {
        avDelegate = nil
        photoOutput = nil
        avCaptureImageOutput = nil
        avPrevLayer = nil
        avInput = nil
        avDevice = nil
        avSession = nil
    }
    
    func requestAuth() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            print("The user has camera access.")
            break
        case .notDetermined:
            self.requestCameraAccess()
            print("The user has not yet been asked for camera access.")
            break
        case .denied:
            print("The user has previously denied access.")
            //self.camDenied()
            break
        case .restricted:
            print("The user can't grant access due to restrictions.")
            break
        @unknown default:
            print("Unknown")
            break
        }
    }
    
    
    func setup() -> Bool {
        print("startSetup")
        self.requestAuth()
        
        self.avSession = AVCaptureSession()
        self.avSession.sessionPreset = .hd1280x720
        
        guard self.avSession != nil else {
            print("Imposible iniciar sesion de captura.")
            return false
        }
        
        self.avDevice = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: .back)
        guard self.avDevice != nil else {
            print("Imposible localizar dispositivo de captura.")
            return false
        }
        
        do {
            self.avInput = try AVCaptureDeviceInput(device: self.avDevice)
        } catch let error {
            print("Error al capturar Input Device:\(error)}" )
            return false
        }
        
        if self.avSession.canAddInput(self.avInput!) {
            self.avSession.addInput(self.avInput)
        } else {
            print("Imposible agregar entrada de video.")
            return false
        }
        
        return true
    }
    
    
    func startVideoCapture(delegate: AVCaptureVideoDataOutputSampleBufferDelegate) {
        guard self.avSession != nil else {
            print("La sesión de captura de video no pudo ser iniciada")
            return
        }
        print("Iniciando AVCaptureVideoDataOutputSampleBufferDelegate")
        self.avSession.beginConfiguration()
        self.avPrevLayer = AVCaptureVideoPreviewLayer(session: (self.avSession!))
        self.avPrevLayer?.frame.size = cameraView.frame.size
        self.avPrevLayer?.videoGravity = .resizeAspectFill
        cameraView.layer.addSublayer(self.avPrevLayer!)
        
        if self.avCaptureImageOutput != nil {
            print("Ya existe una sesión de captura de video")
            return
        }
        
        self.avCaptureImageOutput = AVCaptureVideoDataOutput()
        if self.avSession?.canAddOutput(self.avCaptureImageOutput) ?? false {
            self.avCaptureImageOutput.videoSettings = [(kCVPixelBufferPixelFormatTypeKey as NSString) : NSNumber(value: kCVPixelFormatType_32BGRA)] as [String : Any]
            self.avCaptureImageOutput.alwaysDiscardsLateVideoFrames = true
            self.avSession.addOutput(self.avCaptureImageOutput)
            self.avCaptureImageOutput.setSampleBufferDelegate(delegate,
                                                              queue: DispatchQueue(label: "camera_frame_processing_queue"))
            guard let connection = self.avCaptureImageOutput.connection(with: AVMediaType.video),
                connection.isVideoOrientationSupported else { return }
            connection.videoOrientation = .portrait
            if connection.isVideoStabilizationSupported {
                connection.preferredVideoStabilizationMode = .off
            }
            self.avDevice.configureDesiredFrameRate(24)
            self.avSession.beginConfiguration()
            self.photoOutput = AVCapturePhotoOutput()

            self.photoOutput!.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format: [AVVideoCodecKey : AVVideoCodecType.jpeg])], completionHandler: nil)
            if avSession.canAddOutput(self.photoOutput!) { avSession.addOutput(self.photoOutput!) }
            if self.avDevice != nil {
                    self.setFocusMode(.continuousAutoFocus)

            }
            self.avSession.commitConfiguration()
        } else {
            print("Not available recording method")
        }
        self.avSession.commitConfiguration()
        self.avSession?.startRunning()
    }
    
    
    func requestCameraAccess() {
        AVCaptureDevice.requestAccess(for: .video) { granted in
            if granted {
                print("COOL")
            }
            print("Sin Permiso para uso de camara")
        }
    }
    
    
    func stop() {
        self.avSession?.stopRunning()
    }
    
    
    @IBAction func switchTorch() {
              
              if let device = self.avDevice {
                  do {
                      try device.lockForConfiguration()
                      if self.avDevice.hasTorch {
                          if device.isTorchAvailable {
                              if self.torchMode == .off {
                                  self.avDevice.torchMode = .on
                                  self.torchMode = .on
                              } else {
                                  self.avDevice.torchMode = .off
                                  self.torchMode = .off
                              }
                          }
                      }
                  } catch {
                      print("no se puede bloquear el dispositivo.")
                  }
              }

          }
    
    func isTorchAvailable() -> Bool{
        if let device = self.avDevice {
            do {
                try device.lockForConfiguration()
                if self.avDevice.hasTorch {
                    return true
                }
                
            } catch {
                return false
            }
            
        }
        return false
    }
    
    func captureImage(completion: @escaping (UIImage?) -> Void, center: CGPoint, torch: AVCaptureDevice.TorchMode = .off) {
        guard let captureSession = self.avSession, captureSession.isRunning else {
            print("::: Error al obtener imagen de foto")
            return
        }
        print("::: CaptureImage :0")

        let settings = AVCapturePhotoSettings()
        settings.flashMode = .auto
        do {
            print("::::se prueba si soporta autofoco")

            try self.avDevice.lockForConfiguration()
            self.avDevice.torchMode = torch
            self.avDevice.focusPointOfInterest = center
            self.setFocusMode(.continuousAutoFocus)
            self.avDevice.unlockForConfiguration()
            if let delegCapture = self.avDelegate {
                print("::::Se intenta llamar el delegado de la photo")
                self.photoOutput?.capturePhoto(with: settings, delegate: delegCapture)
                try self.avDevice.lockForConfiguration()
                self.setFocusMode(.continuousAutoFocus)
                self.avDevice.unlockForConfiguration()
                print("::::Se hizo la llamada al delegado de photo")
            } else {
                print ("el delegado no jala")
            }
        } catch let err {
            print("::: Error enfoque")
            print (err)
            
        }
    }
    func setFocusMode(_ mode: AVCaptureDevice.FocusMode) {
        do {
            try self.avDevice.lockForConfiguration()
            if self.avDevice.isFocusModeSupported(mode) {
                self.avDevice.focusMode = mode
                if self.avDevice.isSmoothAutoFocusSupported {
                    self.avDevice.isSmoothAutoFocusEnabled = true
                }
            }
            self.avDevice.unlockForConfiguration()
        } catch let err {
            print("::: Error enfoque")
            print (err)
            
        }
    }
    
}


extension AVCaptureDevice {
    
    func configureDesiredFrameRate(_ desiredFrameRate: Int) {
        
        var isFPSSupported = false
        
        do {
            
                for range in activeFormat.videoSupportedFrameRateRanges {
                    if (range.maxFrameRate >= Double(desiredFrameRate) && range.minFrameRate <= Double(desiredFrameRate)) {
                        isFPSSupported = true
                        break
                    }
                }
            
            if isFPSSupported {
                try lockForConfiguration()
                activeVideoMaxFrameDuration = CMTime(value: 1, timescale: Int32(desiredFrameRate))
                activeVideoMinFrameDuration = CMTime(value: 1, timescale: Int32(desiredFrameRate))
                unlockForConfiguration()
            }
            
        } catch {
            print("lockForConfiguration error: \(error.localizedDescription)")
        }
    }
    

}
