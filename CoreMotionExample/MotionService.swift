//
//  File.swift
//  CoreMotionExample
//
//  Created by Leonardo Gómez Sosa on 15/04/20.
//  Copyright © 2020 Maxim Bilan. All rights reserved.
//

import CoreMotion


class MotionService: NSObject {
    let motionManager = CMMotionManager()
    var timer: Timer!
    let radToGrad = 360 / ( 2 * Double.pi )
    var staticFrames = 0

    static let shared = MotionService()
    
    override init() {
        motionManager.startAccelerometerUpdates()
        motionManager.startDeviceMotionUpdates()
    }
    
    func startMotionService() {
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 1.0 / 24.0, target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
        }
    }
    
    func stopMotionService() {
        timer.invalidate()
        timer = nil
        staticFrames = 0
    }
    
    func resetMotionService(){
        self.stopMotionService()
        self.startMotionService()
    }
    
    @objc func update() {
        if let accelerometerData = motionManager.accelerometerData {
            let xAccel = accelerometerData.acceleration.x
            let yAccel = accelerometerData.acceleration.y
            let zAccel = accelerometerData.acceleration.z
            let normAccel = sqrt( xAccel * xAccel +  yAccel * yAccel + zAccel * zAccel)
            if normAccel > 1.03 {
                staticFrames = 0
                print("=> reset 'cos start acelerating")//, normAccel)
                NotificationCenter.default.post(Notification(name: Notification.Name.isAccelerating))
            }
            
        }
        if let deviceMotion = motionManager.deviceMotion {
            let xRot = deviceMotion.rotationRate.x * radToGrad
            let yRot = deviceMotion.rotationRate.y * radToGrad
            let zRot = deviceMotion.rotationRate.y * radToGrad
            
            let norma = sqrt(xRot * xRot + yRot * yRot + zRot + zRot)
            
            if norma > 20 {
                staticFrames = 0
                print("=> reset 'cos is rotating")//, normAccel)
                NotificationCenter.default.post(Notification(name: Notification.Name.isRotating))
            }
        }
        staticFrames += 1
        
        if staticFrames > 24 {
            NotificationCenter.default.post(Notification(name: Notification.Name.readyTofocus))
        }
        
    }
}

